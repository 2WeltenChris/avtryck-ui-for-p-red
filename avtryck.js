const fs = require('fs')
const path = require('path')
const layouts = require('./layouts.js')
const components = require('./components.js')
const replace = require('@rollup/plugin-replace')
const resolvePath = require('./utils/module-resolver.js')

// This map allows to resolve a component from it's menu path
const componentMap = new Map()

const componentsPackageJson = resolvePath('carbon-components-svelte/package.json')
const componentsModulePath = path.dirname(componentsPackageJson)

const iconsPackageJson = resolvePath('carbon-icons-svelte/package.json')
const iconsPath = path.join(path.dirname(iconsPackageJson), 'lib')

const chartsPackageJson = resolvePath('@carbon/charts-svelte/package.json')
const chartsPath = path.join(path.dirname(chartsPackageJson), 'dist')

let cssFiles

const pluginName = 'Carbon Design'
const carbonDesignPrefix = '@avtryck-ui-for-p-red/'

function traverseComponents (component, callback) {
  for (const slotDefinition of Object.values(component.slots)) {
    const modifiedComponents = []
    slotDefinition.components.forEach((component) => {
      if (component.type === 'component' && component.componentId.startsWith(carbonDesignPrefix)) {
        const result = callback(component)
        if (result) {
          modifiedComponents.push(result)
        }
      } else {
        modifiedComponents.push(component)
      }
      slotDefinition.components = modifiedComponents
      if (component.slots) {
        traverseComponents(component, callback)
      } else if (component.type === 'conditions') {
        component.conditions.forEach(condition => {
          traverseComponents(condition, callback)
        })
      }
    })
  }
}

exports.default = {
  name: pluginName,
  setup (app, installedVersion) {
    console.log('The avtryck-ui (carbon design) plugin gets installed for ' + app.name + '.')

    const publicAvtryckPath = path.join(app.paths.pekfingerRedDir, 'public', 'avtryck-ui')
    const cssPathRelative = path.join('public', 'avtryck-ui', 'css')
    const cssPathAbsolute = path.join(app.paths.pekfingerRedDir, cssPathRelative)
    const fontsPathRelative = path.join('public', 'avtryck-ui', 'fonts')
    const fontsPathAbsolute = path.join(app.paths.pekfingerRedDir, fontsPathRelative)
    const carbonComponentsJson = resolvePath('carbon-components/package.json')
    const cssVarsPath = path.join(path.dirname(carbonComponentsJson), '/scss/globals/scss/vendor/@carbon/themes/scss/compat/generated/_themes.scss')

    
    // Init paths, replace within css and copy css files to public folder
    cssFiles = {
      dynamic: path.join(cssPathRelative, 'all.css'),
      all: path.join(cssPathRelative, 'all.css'),
      white: path.join(cssPathRelative, 'white.css'),
      g10: path.join(cssPathRelative, 'g10.css'),
      g80: path.join(cssPathRelative, 'g80.css'),
      g90: path.join(cssPathRelative, 'g90.css'),
      g100: path.join(cssPathRelative, 'g100.css')
    }
    
    if (installedVersion.aggregated < 1002002 ) {
      // rebuild public folder as css now has custom variables included
      fs.rmSync(publicAvtryckPath, { recursive: true, force: true})
    }
    
    if (!fs.existsSync(publicAvtryckPath)) {
      if (!fs.existsSync(cssPathAbsolute)) {
        fs.mkdirSync(cssPathAbsolute, { recursive: true })
      }
      // get variable css color file
      const cssVars = (fs.existsSync(cssVarsPath)) ? fs.readFileSync(cssVarsPath, { encoding: 'utf8' }) : ''
      fs.readdirSync(path.join(componentsModulePath, 'css')).forEach((file) => {
        if (file.endsWith('.css')) {
          const css = fs.readFileSync(path.join(componentsModulePath, 'css', file)).toString()
          const fontsPathForUrl = '/' + fontsPathRelative.replaceAll('\\', '/')
          let rewrittenCss = css.replaceAll(/url\("https:\/\/[^\/]*\/common\/carbon\/plex\/fonts/g, 'url("' + fontsPathForUrl) // eslint-disable-line no-useless-escape
          
          // add css variables -> Attention: full support only with all.css, especially if using global custom css in web-app
          // to ensure using the color code in avtryck components (e.g. disabled) in a single theme use, we have to add those variables here again
          let themeCssVars = ''
          // all.css has already the variables
          if (cssVars && file !== 'all.css') {
            const createRegexp = (name) => new RegExp(`\\$${name}:\\s*\\((?<css>(?:\\s|.)*?)\\)\\s*!default`, 'g')
            themeCssVars = [...cssVars.matchAll(createRegexp(file.slice(0, -4)))]?.[0]?.groups?.css || ''
            if (!themeCssVars) {
              // use g90 as default if no match found
              themeCssVars = [...cssVars.matchAll(createRegexp('g90'))]?.[0]?.groups?.css || ''
            }
            if (themeCssVars) {
              let cssMin = themeCssVars.trim().split('\n')
              cssMin = cssMin.map(el => '--cds-' + el.replaceAll(' ', '').slice(0,-1)).join(';')
              rewrittenCss += '\n:root{' + cssMin + '}'
            }
          }
          fs.writeFileSync(path.join(cssPathAbsolute, file), rewrittenCss)
        }
      })
      // Copy fonts to public folder
      const fontsPackageJson = resolvePath('@ibm/plex/package.json')
      const fontsModulePath = path.dirname(fontsPackageJson)
      fs.cpSync(fontsModulePath, fontsPathAbsolute, { recursive: true }, () => { /* Ignore */ })
    }
  },
  configure: {
    layouts (_layouts, add) {
      // TODO: Add blank layout
      for (const [componentPath, component] of Object.entries(layouts)) {
        // id is usually the menu path, if in the ui the menu path should be changed, use the componentUI function with the component.menuPathes property
        const menuPath = component.id
        const filePath = componentPath.startsWith('./') ? path.join('avtryck-ui-for-p-red', 'layouts', componentPath) : path.join('carbon-components-svelte', 'src', componentPath)
        add(menuPath, filePath)
        componentMap.set(menuPath, componentPath)
      }
    },
    components (_components, add) {
      // TODO: We should extract the type information from ts annotations (generally - not only for this plugin).
      // TODO: We need a helper for icons!
      for (const [componentPath, component] of Object.entries(components)) {
        // id is usually the menu path, if in the ui the menu path should be changed, use the componentUI function with the component.menuPathes property
        const menuPath = component.id
        const filePath = componentPath.startsWith('./') ? path.join('avtryck-ui-for-p-red', 'components', componentPath) : path.join('carbon-components-svelte', 'src', componentPath)
        add(menuPath, filePath)
        componentMap.set(menuPath, componentPath)
      }
    },
    componentUI (details) {
      if (details.componentId.startsWith(carbonDesignPrefix)) {
        const id = details.componentId.substring(carbonDesignPrefix.length + pluginName.length + 1)
        const config = components[componentMap.get(id)] || layouts[componentMap.get(id)]
        if (config) {
          // Add description
          if (config.description) {
            details.description = config.description
          }
          // Add alternative menu pathes
          if (config.menuPathes?.length) {
            // add prefix + pluginname to ensure same handling as if the componentId was used
            details.menuPathes = config.menuPathes.map(path => carbonDesignPrefix + pluginName + '/' + path)
          }
          // set alternative name
          if (config.name) {
            details.name = config.name
          }
          // Hide properties
          if (config.properties?.hide) {
            config.properties.hide.forEach((property) => {
              delete details.exports[property]
            })
          }
          // Add properties
          if (config.properties?.add) {
            details.exports = { ...details.exports, ...config.properties.add }
          }
          // Add id + class for all components if it is not an existing property
          if (!details.exports.id) {
            details.exports.id = { value: '', bind:false }
          }
          if (!details.exports.class) {
            details.exports.class = { value: '', bind:false }
          }
          // Describe properties (placeholder / default value)
          if (config.properties?.describe) {
            for (const [property, description] of Object.entries(config.properties.describe)) {
              if (typeof description.placeholder !== 'undefined') {
                details.exports[property].placeholder = description.placeholder
                if (details.exports[property].placeholder.constructor === Array) {
                  details.exports[property].placeholderType = 'ArrayExpression'
                }
              }
              if (typeof description.defaultValue !== 'undefined') {
                details.exports[property].defaultValue = description.defaultValue
              }
            }
          }
          // Hide slots
          if (config.slots?.hide) {
            const slots = []
            details.slots.forEach((slot) => {
              if (!config.slots.hide.includes(slot.name)) {
                slots.push(slot)
              }
            })
            details.slots = slots
          }
          // Add slots
          if (config.slots?.add) {
            config.slots.add.forEach(newSlot => details.slots.push(newSlot))
          }
          // Add events
          if (config.events?.add) {
            Object.keys(config.events.add).forEach(newEventName => {
              details.eventHandlers[newEventName] = config.events.add[newEventName]
            })
          }
          // Hide events
          if (config.events?.hide) {
            const eventHandlers = {}
            for (const [eventName, defaultBehaviour] of Object.entries(details.eventHandlers)) {
              if (!config.events.hide.includes(eventName)) {
                eventHandlers[eventName] = defaultBehaviour
              }
            }
            details.eventHandlers = eventHandlers
          }
        }
      }
    },
    // template (hbsTemplate, context) {
    //   // TODO: Add helper function if needed
    // },
    page (structure, context) {
      // Transform CD components if necessary
      // Use pathes to set the 'correct' import path if the component need additional imports.
      const pathes = { 
        icons: 'carbon-icons-svelte/lib',
        carbonComponents: 'carbon-components-svelte/src'
      }
      const doTransform = (component) => {
        const id = component.componentId.substring(carbonDesignPrefix.length + pluginName.length + 1)
        const componentPath = componentMap.get(id)
        const config = components[componentPath] || layouts[componentPath]
        if (config?.transform) {
          return config.transform(component, context, pathes)
        } else {
          return component
        }
      }
      doTransform(structure) // Transform layout
      traverseComponents(structure, doTransform) // Transform components
    },
    vite (config) {
      // Workaround: Inline frames currently won't work without the next two lines
      config.server.fs.allow.push(__dirname)
      config.server.fs.allow.push(fs.realpathSync(componentsModulePath))
      config.server.fs.allow.push(fs.realpathSync(iconsPath))
      config.server.fs.allow.push(fs.realpathSync(chartsPath))
      // config.server.fs.allow.push(fs.realpathSync(chartsCss))
    },
    build (config) {
      const build = config.build
      build.rollupOptions = build.rollupOptions || {}
      build.rollupOptions.plugins = build.rollupOptions.plugins || []
      // Workaround for carbon-charts to work (see https://github.com/carbon-design-system/carbon-charts/issues/1070#issuecomment-883794826)
      build.rollupOptions.plugins.push(replace({
        preventAssignment: true,
        "process.env.NODE_ENV": JSON.stringify("production"),
      }))
    },
    // renderingContext (context, structure) {
    // },
    wrapperContext (context) {
      if (context.page.content.includes('avtryck-ui-for-p-red')) {
        // This adds the theme CSS for SSR (needs trailing '/')
        let cssFile = (cssFiles[context.globals['carbon-theme']] || cssFiles.all) 
        if (!cssFile.startsWith('/')) {
          // Filepath is absolute -> add '/'
          cssFile = '/' + cssFile
        }
        const importStatements = '\n<link rel="preload" as="style" href="' + cssFile + '" />\n<link rel="stylesheet" href="' + cssFile + '" />'
        if (!context.head) {
          context.head = importStatements
        } else {
          context.head += importStatements
        }

   
        
        // This ensures that custom styles are also applied to the SSR version (we could otherwhise not manipulate the head).
        // Attention: This works only with the all.css! -> carbon design single theme file (like g90) transforms the variables to the color in the css.
        let customCss = context.globals['carbon-css-variables']
        if (customCss) {
          try {
            if (typeof context.globals['carbon-css-variables'] === 'str') {
              customCss = JSON.parse(customCss)
            }
            delete customCss.warning
            Object.keys(customCss).forEach(varName => context.style.html += varName + ':' + customCss[varName] + ';')
          } catch (e) {
            console.log('[avtryck-ui] Could\'nt add custom css variables from global', e)
          }
        }
      }
    },
    // add unremovable global variables to the web-app global
    globals: () => {
      return { 
        "carbon-theme": {
          value: 'all', 
          type: 'str',
          placeholder: 'all || white || g10 || g80 || g90 || g100'
        },
        "carbon-css-variables": {
          value: "{\"warning\":\"only working with 'all'/'dynamic' theme. (e.g. '--cds-interactive-01': 'red'\"}",
          type: 'json',
          placeholder: "{\"warning\":\"only working with 'all'/'dynamic' theme. (e.g. '--cds-interactive-01': 'red'\"}"
        }
      }
    }
  }
}
