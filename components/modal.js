const { wrapIcon } = require('./utils/icon')

module.exports = {
  // TODO: Add components.
  'Modal/Modal.svelte': {
    id: 'Modal/Modal',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Modal)\n\n[Icon List](https://carbon-icons-svelte.onrender.com)',
    transform (component, context, pathes) {
      // Wrap icon
      wrapIcon(component.properties.primaryButtonIcon.value, context, pathes)
      return component
    }
  },
  // ComposedModal can maybe simplified with transform....
  // 'ComposedModal/ComposedModal.svelte': {
  //   id: 'Modal/Composed modal/Composed modal',
  //   description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/ComposedModal)'
  // },
  // 'ComposedModal/ModalHeader.svelte': {
  //   id: 'Modal/Composed modal/Composed modal header',
  //   description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/ComposedModal)'
  // }
  // 'ComposedModal/ModalBody.svelte': {
  //   id: 'Modal/Composed modal/Composed modal body',
  //   description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/ComposedModal)'
  // },
  // 'ComposedModal/ModalFooter.svelte': {
  //   id: 'Modal/Composed modal/Composed modal footer',
  //   description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/ComposedModal)'
  // },
}
