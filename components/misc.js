const { wrapIcon } = require('./utils/icon')

module.exports = {
  'Loading/Loading.svelte': {
    id: 'Miscellaneous/Loading',
    description: '\n\nA loading spinner.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Loading)',
  },
  'Tag/Tag.svelte': {
    id: 'Miscellaneous/Tag',
    description: '\n\nA tag element.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Tag)',
    properties: {
      add: {
        name: { placeholder: '"Enter a tag description"', mandatory: false, unbindable: true }
      }
    },
    slots: {
      hide: [ 'default', 'icon' ]
    },
    transform (component) {
      // Delete name property and move it to the default slot
      let name = component.properties.name.value
      if (!name) {
        name = 'Please add a tag name'
      }
      delete component.properties.name
      component.slots = {
        default: {
          components: [ { type: 'code', code: '{' + name + '}' } ],
          use: {}
        }
      }
      return component
    }
  },
  'OverflowMenu/OverflowMenu.svelte': {
    id: 'Miscellaneous/OverflowMenu/Overflow menu',
    description: '\n\nThe container for overflow menu items.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/OverflowMenu)\n\n[Icon List](https://carbon-icons-svelte.onrender.com)',
    transform (component, context, pathes) {
      // Wrap icon
      wrapIcon(component.properties.icon.value, context, pathes)
      return component
    }
  },
  'OverflowMenu/OverflowMenuItem.svelte': {
    id: 'Miscellaneous/OverflowMenu/Overflow menu item',
    description: '\n\nAn overflow menu item.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/OverflowMenu)',
  }
}
