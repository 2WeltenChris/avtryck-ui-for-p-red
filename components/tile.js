module.exports = {
  'Tile/Tile.svelte': {
    id: 'Tile/Simple Tile',
    description: '\n\nThe default tile. [Official documentation](https://carbon-components-svelte.onrender.com/components/Tile)'
  },
  'Tile/ClickableTile.svelte': {
    id: 'Tile/Clickable tile',
    description: '\n\nA clickable tile. [Official documentation](https://carbon-components-svelte.onrender.com/components/ClickableTile)'
  },
  'Tile/ExpandableTile.svelte': {
    id: 'Tile/Expandable tile',
    description: '\n\nAn expandable tile. [Official documentation](https://carbon-components-svelte.onrender.com/components/ExpandableTile)'
  },

  './tile/SelectableTileGroup.svelte': {
    id: 'Tile/Selectable tiles/Selectable tile group',
    description: '\n\nThe container for multiple selectable tiles. [Official documentation](https://carbon-components-svelte.onrender.com/components/SelectableTile)'
  },
  'Tile/SelectableTile.svelte': {
      id: 'Tile/Selectable tiles/Selectable tile',
      description: '\n\nA selectable tile. [Official documentation](https://carbon-components-svelte.onrender.com/components/SelectableTile)'
  }
}