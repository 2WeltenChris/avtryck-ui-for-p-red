module.exports = {
  'DataTable/DataTable.svelte': {
    id: 'Table/Table',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)',
    properties: {
      describe: {
        headers: {
          placeholder: 'Array of objects ( {"key": "unique id", "value": "header name"} )',
          defaultValue: '[]'
        }
      }
    },
  },
  'DataTable/Toolbar.svelte': {
    id: 'Table/Toolbar/Toolbar frame',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)'
  },
  'DataTable/ToolbarContent.svelte': {
    id: 'Table/Toolbar/Content',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)'
  },
  'DataTable/ToolbarSearch.svelte': {
    id: 'Table/Toolbar/Search',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)',
  },
  'DataTable/ToolbarMenu.svelte': {
    id: 'Table/Toolbar/Menu',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)',
    // ToolbarMenu is a wrapped OverflowMenu -> adding the Overflow exported keys
    properties: {
      add: {
        open: { placeholder:  ['true', 'false'], defaultValue: 'false', mandatory: false, description: 'Set to true to open the menu.' },
        // icon is fix
        // icon: { placeholder:  "OverflowMenuVertical", defaultValue: 'OverflowMenuVertical', mandatory: false, description: 'Specify the icon to render.' },
        // menuRef is fix
        // menuRef: { placeholder:  'HTMLUListElement', defaultValue: 'null', mandatory: false, description: 'Obtain a reference to the overflow menu element.' },
        direction: { placeholder:  ['"top"', '"bottom"'], defaultValue: '"bottom"', mandatory: false, description: 'Specify the direction of the overflow menu relative to the button.' },
        light: { placeholder:  ['true', 'false'], defaultValue: 'false', mandatory: false, description: 'Set to true to enable the light variant.' },
        // flipped is fix
        // flipped: { placeholder:  ['true', 'false'], defaultValue: 'false', mandatory: false, description: 'et to true to flip the menu relative to the button.' },
        iconClass: { placeholder:  '', defaultValue: '', mandatory: false, description: 'Specify the icon class.' },
        iconDescription: { placeholder: '"Open and close list of options"', defaultValue: '', mandatory: false, description: 'Specify the ARIA label for the icon.' },
      }
    }, transform (component) {
      const additionalKeys = ['open', 'direction', 'light', 'iconClass', 'iconDescription']
      additionalKeys.forEach(k => {
        const prop = component.properties[k]
        // remove property if no value or default value to ensure getting default value from nested component
        if (prop && (prop.value === '' || prop.value === prop.defaultValue)) {
          delete component.properties[k]
        }
      })
      return component
    }
  },
  'DataTable/ToolbarMenuItem.svelte': {
    id: 'Table/Toolbar/Menu Item',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)',
    slots: {
      hide: ['default']
    },
    properties: {
      add: { 
        text: { placeholder: '', defaultValue: "'Unknown menu item'", mandatory: true, description: "Set a menu label text." },
        href: { placeholder:  '', defaultValue: '', mandatory: false, description: 'Specify the `href` attribute if the item is a link.' },
        primaryFocus: { placeholder:  ['true', 'false'], defaultValue: 'false', mandatory: false, description: 'Set to `true` if the item should be focused when opening the menu' },
        disabled: { placeholder:  ['true', 'false'], defaultValue: 'false', mandatory: false, description: 'Set to `true` to disable the item.' },
        hasDivider: { placeholder:  ['true', 'false'], defaultValue: 'false', mandatory: false, description: 'Set to `true` to include a divider.' },
        danger: { placeholder:  ['true', 'false'], defaultValue: 'false', mandatory: false, description: 'Set to `true` to use the danger variant.' },
      }
    },
    transform (component) {
      // Delete text property and move it to the default slot
      const text = component.properties.text.value
      delete component.properties.text
      component.slots = {
        default: {
          components: [
            {
              type: 'code',
              code: '{' + text + '}'
            }
          ],
          use: {}
        }
      }
      const additionalKeys = ['href', 'primaryFocus', 'disabled', 'hasDivider', 'danger']
      additionalKeys.forEach(k => {
        const prop = component.properties[k]
        // remove property if no value or default value to ensure getting default value from nested component
        if (prop && (prop.value === '' || prop.value === prop.defaultValue)) {
          delete component.properties[k]
        }
      })
      return component
    }
  },
  'DataTable/ToolbarBatchActions.svelte': {
    id: 'Table/Toolbar/Batch actions',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)'
  },
  'Pagination/Pagination.svelte': {
    id: 'Table/Pagination',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DataTable)'
  }
}
