/** 
 * transform the structure from UI to carbon style
 * UI: Tabs 
 *      Tab 1 -> Slot with Content
 *      Tab 2 -> Slot with Content
 * 
 * carbon: 
 * <Tabs>
 *  <Tab label="Tab label 1" />
 *  <Tab label="Tab label 2" />
 *  <Tab label="Tab label 3" />
 *  <svelte:fragment slot="content">
 *    <TabContent>Content 1</TabContent>
 *    <TabContent>Content 2</TabContent>
 *    <TabContent>Content 3</TabContent>
 *  </svelte:fragment>
 * </Tabs>
 */

module.exports = {
  'Tabs/Tabs.svelte': {
    id: 'Navigation/Tabs/Tabs Container',
    description: '\n\nThe container for Tab components.\n\nDefault slot content must be "Tab" or each/condition with a "Tab" component [Official documentation](https://carbon-components-svelte.onrender.com/components/Tabs)',
    slots: {
      hide: [ 'default' ]
    },
    transform (component, context) {
      // Add TabContent import
      context.imports.add(`import TabContent from 'carbon-components-svelte/src/Tabs/TabContent.svelte'`)

      // restore default slot
      component.slots.default = {
        components: [],
        use: {}
      }
      
      const contentSlotComponents = []
      const isTabComponent = (c) => c.type === 'component' && c.name === 'Tab'

      const createTabContentWrap = (wrappedSlotContent) => {
        return {
          type: 'component',
          name: 'TabContent',
          componentId: '@avtryck-ui-for-p-red/Carbon Design/Navigation/Tabs/_TabContent',
          properties: {},
          eventHandlers: {},
          slots: {
            default: {
              components: wrappedSlotContent
            }
          }
        } 
      }

      // TODO: there is maybe some overhead from the previous code -> refactoring some day...
      // copy all content into default slot. This will contain each/condition or the Tab component itself
      // the slot content for the Tab component will be deleted later by transforming the Tab component
      component.slots.default.components = [...component.slots.content.components]

      // Only each / condition / Tab allowed
      component.slots.content.components.forEach(slotComp => {
        // special cases each/condition
        if (slotComp.type === 'each') {
          const tabContent = structuredClone(slotComp)
          // warning: Default each slot name is "components" -> components.components for slot content
          const eachComponentSlotComponents = tabContent.slots.components.components
          if (isTabComponent(eachComponentSlotComponents?.[0])) {
            const wrappedComponents = createTabContentWrap(eachComponentSlotComponents[0].slots.default.components)
            tabContent.slots.components.components.push(wrappedComponents)
            // Remove Tab component
            tabContent.slots.components.components.shift()
            contentSlotComponents.push({ ...tabContent })
          }
        } else if (slotComp.type === 'conditions') {
          const tabContent = structuredClone(slotComp)

          // prepare TabContent 
          tabContent.conditions = tabContent.conditions.map(cond => {
            let thisConditionTabContents = []
            cond.slots.condition.components.forEach(tab => {
              if (isTabComponent(tab)) {
                const wrappedComponents = createTabContentWrap(tab.slots.default.components)
                thisConditionTabContents.push(wrappedComponents)
              } else {
                console.error('[Avtryck] Invalid component type "' + slotComp.type + '" in TabComponents condition slot.')
              }             
            })
            cond.slots.condition.components = thisConditionTabContents
            return cond
          })
          contentSlotComponents.push({ ...tabContent })    
        } else if (isTabComponent(slotComp)) {
            // set <Tabs> <Tab label="Tab label 1" /> </Tabs> (TabComponents default slot)
            contentSlotComponents.push(createTabContentWrap(slotComp.slots.default.components))
        } else {
          console.error('[Avtryck] Invalid component type "' + slotComp.type + '" in TabComponents slot.')
        }
      })
    
      // add TabContent component
      component.slots.content.components = contentSlotComponents

      return component
    }
  },
  'Tabs/Tab.svelte': {
    id: 'Navigation/Tabs/Tab',
    description: '\n\nThe Tab component. [Official documentation](https://carbon-components-svelte.onrender.com/components/Tabs)',
    slots: {
      add: [ {exposes: [], name: "default"} ]
    },
    transform (component, _context, _pathes) {
      // remove not existing slot (which will be added to TabComponent)
      component.slots = []

      return component
    }
  },
  'Tabs/TabContent.svelte': {
    id: 'Navigation/Tabs/_TabContent',
    description: '\n\nThe active Tab content. [Official documentation](https://carbon-components-svelte.onrender.com/components/Tabs)'
  },
  'Breadcrumb/Breadcrumb.svelte': {
    id: 'Navigation/Breadcrumbs/Breadcrumb container',
    description: '\n\nThe container for breadcrumb items. [Official documentation](https://carbon-components-svelte.onrender.com/components/Breadcrumb)'
  },
  'Breadcrumb/BreadcrumbItem.svelte': {
    id: 'Navigation/Breadcrumbs/Breadcrumb item',
    description: '\n\nThe breadcrumb item. [Official documentation](https://carbon-components-svelte.onrender.com/components/Breadcrumb)',
    slots: {
      hide: [ 'default' ]
    },
    properties: {
      add: {
        label: { placeholder: '', mandatory: true, unbindable: true }
      }
    },
    transform (component) {
      // Delete label property and move it to the default slot
      let label = component.properties.label.value
      if (label) {
        if (!component.eventHandlers.click.value && component.properties.href.value) {
          // set the href value into an event to ensure using the current frame
          component.eventHandlers.click.value = component.properties.href.value
          component.eventHandlers.click.type = 'client.load'
          component.properties.href.value = '"#"'
        }
        // make the breadcrumb clickable if not current page
        component.properties.href.value = component.properties.isCurrentPage.value + ' ? "" : "#"'

        component.slots = {
          default: {
            components: [
              {
                type: 'code',
                code: '{' + label + '}'
              }
            ],
            use: {}
          }
        }
      }
      delete component.properties.label
      return component
    }
  }
}
