const { wrapIcon } = require('./utils/icon')

const handleDisable = (props, propsToDisable = []) => {
  if (props?.disabled?.value && props.disabled.value !== 'false') {
    // auto disable e.g. href
    propsToDisable.forEach(p => {
      if (props[p]?.value) {
        props[p].value = props.disabled.value + '? "" : (' + (props[p].value) + ')'
      }
    })
    props.class = props.class || {}
    props.class.value = props.class.value || '""'
    props.class.value = '(' + props.disabled.value + '? "avtryck-disabled " : "") + ' + props.class.value
  }
}

module.exports = {
  'UIShell/HeaderNavItem.svelte': {
    id: 'App: Header Navigation/Item',
    description: 'A navigation item for the header menu. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)',    
    properties: {
      hide: ['ref'],
      add: {
        disabled: { placeholder: ['true', 'false'], defaultValue: 'false', mandatory: false}
      }
    },
    transform (component, context, pathes) {
      handleDisable(component.properties, ['href'])
      return component
    }
  },
  'UIShell/HeaderNavMenu.svelte': {
    id: 'App: Header Navigation/Menu',
    description: 'A submenu for the header menu. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)',
    properties: {
      hide: ['ref']
    }
  },
  './shell/Search.svelte': {
    id: 'App: Header Actions/Search',
    description: 'A search component for the App header. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)'
  },
  'UIShell/HeaderGlobalAction.svelte': {
    id: 'App: Header Actions/Action',
    description: 'A header action button in the utilities area. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)\n\n[Icon List](https://carbon-icons-svelte.onrender.com)',
    properties: {
      hide: ['ref'],
      add: {
        // Add some Button props (selected is not working yet?)
        iconDescription: { placeholder: '', mandatory: false }, 
        tooltipAlignment: { placeholder: ['"start"', '"center"', '"end"'], defaultValue: '"center"', mandatory: false },
        disabled: { placeholder: ['true', 'false'], defaultValue: 'false', mandatory: false }
      }
    },
    slots: {
      hide: ['default']
    },
    transform (component, context, pathes) {
      // Wrap icon
      wrapIcon(component.properties.icon.value, context, pathes)
      return component
    }
  },
  'UIShell/HeaderAction.svelte': {
    id: 'App: Header Actions/Menu',
    description: 'A header menu button in the utilities area. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)\n\n[Icon List](https://carbon-icons-svelte.onrender.com)',
    properties: {
      hide: ['ref', 'transition'],
      describe: {
        icon: {
          placeholder: '',
          defaultValue: 'Switcher'
        }
      },
      add: {
        iconDescription: { placeholder: '', mandatory: false }, 
        tooltipAlignment: { placeholder: ['"start"', '"center"', '"end"'], defaultValue: '"center"', mandatory: false },
        disabled: { placeholder: ['true', 'false'], defaultValue: 'false', mandatory: false }
      }
    },
    slots: {
      hide: ['icon', 'closeIcon', 'text']
    },
    transform (component, context, pathes) {
      // Add HeaderPanelLinks import
      context.imports.add('import HeaderPanelLinks from "' + pathes.carbonComponents + '/UIShell/HeaderPanelLinks.svelte"')
      // Wrap default slot with HeaderPanelLinks
      component.slots.default = {
        components: [
          {
            type: 'html',
            html: '<HeaderPanelLinks>'
          },
          ...component.slots.default.components,
          {
            type: 'html',
            html: '</HeaderPanelLinks>'
          }
        ],
        use: {}
      }
      // Wrap icons
      const props = component.properties
      for (const propertyName of ['icon', 'closeIcon']) {
        let icon = props[propertyName].value
        if (propertyName === 'icon' && props.icon.value === '') {
          icon = 'Switcher' // default icon
        }
        wrapIcon(icon, context, pathes)
      }

      // handle disable
      handleDisable(component.properties, ['isOpen'])
      return component
    }
  },
  'UIShell/HeaderPanelLink.svelte': {
    id: 'App: Header Actions/Menu Entry',
    description: 'A header action menu entry. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)',
    properties: {
      hide: ['ref'],
      add: {
        text: { placeholder: '', mandatory: true, unbindable: true },
        disabled: { placeholder: ['true', 'false'], defaultValue: 'false', mandatory: false }
      }
    },
    slots: {
      hide: ['default']
    },
    transform (component) {
      // Move text to the default slot
      const props = component.properties
      const text = props.text.value
      component.slots = {
        default: {
          components: [
            {
              type: 'code',
              code: '{' + text + '}'
            }
          ],
          use: {}
        }
      }
      delete props.text

      handleDisable(component.properties, ['href'])
      return component
    }
  },
  'UIShell/HeaderPanelDivider.svelte': {
    id: 'App: Header Actions/Menu Divider',
    description: 'A divider for action menus. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)\n\n[Icon List](https://carbon-icons-svelte.onrender.com)',
    properties: {
      add: {
        subject: { placeholder: '', mandatory: false, unbindable: true }
      }
    },
    slots: {
      hide: ['default']
    },
    transform (component) {
      const subject = component.properties.subject.value
      component.slots = {
        default: {
          components: [
            {
              type: 'code',
              code: '{' + subject + '}'
            }
          ],
          use: {}
        }
      }
      delete component.properties.subject
      return component
    }
  },
  './shell/SideNavMenu.svelte': {
    id: 'App: Side Navigation/Menu',
    slots: {
      hide: ['icon']
    },
    transform (component, context, pathes) {
      // Wrap icon
      wrapIcon(component.properties.icon.value, context, pathes)
      return component
    }
  },
  './shell/SideNavEntry.svelte': {
    id: 'App: Side Navigation/Menu Entry',
    transform (component, context, pathes) {
      // Wrap icon
      wrapIcon(component.properties.icon.value, context, pathes)
      // handle disable
      handleDisable(component.properties, ['isOpen'])
      return component
    }
  },
  './shell/SelectableSideNavMenuItem.svelte': {
    id: 'App: Side Navigation/Selectable Menu Entry',
    description: 'A submenu entry for the sidenav that is selectable (checkbox or radio). \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)'
  },
  'UIShell/SideNavDivider.svelte': {
    id: 'App: Side Navigation/Divider',
    description: 'A divider for the sidenav. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/UIShell)',
    properties: {
      hide: ['ref']
    }
  }
}
