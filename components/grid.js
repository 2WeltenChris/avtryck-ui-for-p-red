// TODO: The property transformation ignores any binding. Does this matter?
const wrapRowDescription = `Wrap the page content into a grid structure.

- auto: Wrap the whole slot content into one Column if no Column is existing. 
- Column: Wrap the whole slot content into a Column.
- none: Do not wrap.
`

function transformModeProperty (component, isGrid = false) {
  if (component.properties.mode) {
    const mode = component.properties.mode.value
    delete component.properties.mode
    switch (mode) {
      case '"wide"': {
        break
      }
      case '"condensed"': {
        component.properties.condensed = {
          value: true,
          bind: false
        }
        break
      }
      case '"narrow"': {
        component.properties.narrow = {
          value: true,
          bind: false
        }
        break
      }
      case '"full width"': {
        component.properties.fullWidth = {
          value: true,
          bind: false
        }
        break
      }
      default: {
        if (mode) {
          component.properties.condensed = {
            value: '(' + mode + ') === "condensed"',
            bind: false
          }
          component.properties.narrow = {
            value: '(' + mode + ') === "narrow"',
            bind: false
          }
          if (isGrid) {
            component.properties.fullWidth = {
              value: '(' + mode + ') === "full width"',
              bind: false
            }
          }
        }
      }
    }
  }
}

function transformGutterProperty (component) {
  if (component.properties.gutter) {
    const gutter = component.properties.gutter.value
    delete component.properties.gutter
    switch (gutter) {
      case '"default"': {
        break
      }
      case '"none"': {
        component.properties.noGutter = {
          value: true,
          bind: false
        }
        break
      }
      case '"left-only"': {
        component.properties.noGutterRight = {
          value: true,
          bind: false
        }
        break
      }
      case '"right-only"': {
        component.properties.noGutterLeft = {
          value: true,
          bind: false
        }
        break
      }
      default: {
        if (gutter) {
          component.properties.noGutter = {
            value: '(' + gutter + ') === "none"',
            bind: false
          }
          component.properties.narrow = {
            value: '(' + gutter + ') === "narrow"',
            bind: false
          }
        }
      }
    }
  }
}

module.exports = {
  'Grid/Grid.svelte': {
    id: 'Grid/Grid Container',
    description: 'A grid container with rows and columns for content.\nNeeds at least one row, which will be added automatically with the default value if not set.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Grid)',
    properties: {
      hide: ['as', 'condensed', 'narrow', 'noGutter', 'noGutterLeft', 'noGutterRight'],
      add: {
        mode: { placeholder: '', mandatory: false, unbindable: true },
        gutter: { placeholder: '', mandatory: false, unbindable: true }
      },
      describe: {
        mode: {
          placeholder: ['"wide"', '"condensed"', '"narrow"', '"full width"'],
          defaultValue: '"wide"'
        },
        gutter: {
          placeholder: ['"default"', '"none"', '"right-only"', '"left-only"'],
          defaultValue: '"default"'
        }
      }
    },
    transform (component) {
      // Transform mode property
      transformModeProperty(component, true)

      // Transform gutter property
      transformGutterProperty(component)

      // A Grid always needs at least one row
      if (!component.slots.default.components.find(comp => comp.componentId === '@avtryck-ui-for-p-red/Carbon Design/Grid/Row')) {
        component.slots.default.components = [
          {
            type: 'component',
            name: 'Row',
            componentId: '@avtryck-ui-for-p-red/Carbon Design/Grid/Row',
            properties: {},
            eventHandlers: {},
            slots: {
              default: {
                components: component.slots.default.components
              }
            }
          }
        ]
      }
      delete component.properties.mode
      delete component.properties.gutter

      return component
    }
  },
  'Grid/Row.svelte': {
    id: 'Grid/Row',
    description: 'A row in a grid component.\n\nAlways needs a column, which will automatically added with default options if not added in the slot. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Grid)',
    properties: {
      hide: ['as', 'condensed', 'narrow', 'noGutter', 'noGutterLeft', 'noGutterRight'],
      add: {
        mode: { placeholder: '', mandatory: false, unbindable: true },
        gutter: { placeholder: '', mandatory: false, unbindable: true},
        wrap: { placeholder: ['auto', 'Column', 'none'], mandatory: false, defaultValue: 'auto', description: wrapRowDescription, unbindable: true },
      },
      describe: {
        mode: {
          placeholder: ['"wide"', '"condensed"', '"narrow"'],
          defaultValue: '"wide"'
        },
        gutter: {
          placeholder: ['"default"', '"none"', '"right-only"', '"left-only"'],
          defaultValue: '"default"'
        }
      }
    },
    transform (component) {
      // Transform mode property
      transformModeProperty(component)

      // Transform gutter property
      transformGutterProperty(component)     

      const defaultComponents = component.slots.default.components
      let wrap = component.properties.wrap?.value
      if (!['auto', 'Column', 'none'].includes(wrap)) {
        wrap = 'none'
      }
      if (wrap === 'Column' || (wrap === 'auto' && !component.slots.default.components.find(comp => comp.componentId === '@avtryck-ui-for-p-red/Carbon Design/Grid/Column' ))) {
        // Wrap Row slot components into a column
        component.slots.default.components = [{
          type: 'component',
          name: 'Column',
          componentId: '@avtryck-ui-for-p-red/Carbon Design/Grid/Column',
          properties: {},
          eventHandlers: {},
          slots: {
            default: {
              components: defaultComponents
            }
          }
        }]
      }
      delete component.properties.wrap
      delete component.properties.mode
      delete component.properties.gutter

      return component
    }
  },
  'Grid/Column.svelte': {
    id: 'Grid/Column',
    description: 'A column element. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Grid)',
    properties: {
      hide: ['as', 'noGutter', 'noGutterLeft', 'noGutterRight'],
      add: {
        gutter: { placeholder: '', mandatory: false, unbindable: true }
      },
      describe: {
        gutter: {
          placeholder: ['"default"', '"none"', '"right-only"', '"left-only"'],
          defaultValue: '"default"'
        }
      }
    },
    transform (component) {
      // Transform gutter property
      transformGutterProperty(component)
      delete component.properties.gutter

      return component
    }
  }
}
