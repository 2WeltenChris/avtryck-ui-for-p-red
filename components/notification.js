module.exports = {
  'Notification/InlineNotification.svelte': {
    id: 'Notification/InlineNotification',
    description: '\n\nNotification that appears inline.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/InlineNotification)',
  }
}