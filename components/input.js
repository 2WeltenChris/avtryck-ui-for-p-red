const { wrapIcon } = require('./utils/icon')
const { addValidationVariables } = require('./utils/validatorHelper')
const { randomUUID } = require('crypto')

// Information on the different usages of Select, Dropdown and Combobox.
const differenceSelectDropdownCombobox = `
### Dropdown versus Select

Dropdown and select components have functionality and style differences.

The underlying code of a dropdown component is styled to match the design system, while the select component’s appearance is determined by the browser being used.

Use a dropdown component in forms, to select multiple options at a time and to filter or sort content on a page. The select dropdown does not have filtering or multiselect functionality.

Use a dropdown component if most of your experience is form based. Custom dropdowns can be used in these situations, but the native select works more easily with a native form when submitting data.

Use a select component if your experience will be frequently used on mobile. The native select dropdown uses the native control for the platform which makes it easier to use.

If you are using a dropdown in a modal, this can lead to UI errors (https://github.com/carbon-design-system/carbon-components-svelte/issues/1619). Use a select till this error is fixed.

### Dropdown versus Combo box

While the dropdown and combo box look similar they have different functions.

With a dropdown list, the selected option is always visible, and the other options are visible by clicking and opening the list.
A combo box is a combination of a standard list box or a dropdown list that lets users type inside the input field to find an option that matches their inputted value.
`

const addInputValidator = (component, context) => {
  // a-ui validate on blur (carbon only shows custom errors)
  // add neccessary properties / events
  if (!component.eventHandlers.blur?.value) {
    component.eventHandlers.blur = { value: '', type: 'custom' }
  }
  if (!component.eventHandlers.keyup?.value) {
    component.eventHandlers.keyup = { value: '', type: 'custom' } 
  }
  // must set a id to check for errors later
  let id = component.properties.id?.value
  if (!id) {
    // uuid is neccessary as Date.now() may not be unique
    id = '"a_ui_input_' + randomUUID() + '"'.replaceAll('-', '_')
    component.properties.id = { value: id}
  } 
  addValidationVariables(context)
  // use forceBefore to ensure always adding our blur event, even if user uses a custom blur event
  component.eventHandlers.blur.forceBefore = true
  component.eventHandlers.blur.before = '_avtryck_ui_input_validator(' + id + ')'
  component.eventHandlers.keyup.forceBefore = true
  component.eventHandlers.keyup.before = '_avtryck_ui_input_validator(' + id + ')'
  // use our validation and the (existing?) defined one
  if (!component.properties.invalid?.value) {
    component.properties.invalid =  { value: '_avtryck_ui_input_validation[' + id + ']'}
  } else {
    component.properties.invalid.value = '_avtryck_ui_input_validation[' + id + '] || ' + component.properties.invalid.value
  }
  // show error message
  if (!component.properties.invalidText?.value) {
    component.properties.invalidText =  { value: '_avtryck_ui_input_validation[' + id + ']' }
  } else {
    component.properties.invalidText.value = '_avtryck_ui_input_validation[' + id + '] || ' + component.properties.invalidText.value
  }
}


module.exports = {
  'Button/Button.svelte': {
    id: 'Input/Button',
    // added alternative pathes
    menuPathes: ['Input/Buttons/'],
    description: 'A button. Needs either a name or an icon.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Button)\n\n[Icon List](https://carbon-icons-svelte.onrender.com)',
    properties: {
      hide: ['tabindex', 'as', 'type', 'ref'],
      add: {
        name: { placeholder: '', mandatory: false, unbindable: true },
        iconSpinning: { placeholder: ['false', 'true'], defaultValue: 'false', mandatory: false, description: "Rotate the icon.\n\nUseful if user button indicates a waiting sequence (Renew/Hourglass)." }
      }
    },
    slots: {
      hide: ['default', 'icon']
    },
    events: {
      hide: []
    },
    transform (component, context, pathes) {
      const props = component.properties
      const icon = props.icon.value
      // Delete name property and move it to the default slot
      let name = props.name.value
      if (!name && !icon) {
        name = "Please add a name and/or an icon!"
      }
      delete props.name
      if (name) {
        component.slots = {
          default: {
            components: [
              {
                type: 'code',
                code: '{' + name + '}'
              }
            ],
            use: {}
          }
        }
      } else if (!name && icon && !props.iconDescription.value) {
        // icon only must have an description (which is undefined -> add icon name if empty)
        props.iconDescription.value = '"' + icon + '"'
      }
      // Wrap icon
      wrapIcon(icon, context, pathes)
      if (props.iconSpinning?.value && props.iconSpinning.value !== 'false') {
        props.class = props.class || {}
        props.class.value = props.class.value || ''
        props.class.value = `(${props.iconSpinning.value} ? "spinning" : "") ${props.class.value}`
      }
      return component
    }
  },
  'Button/ButtonSet.svelte': {
    id: 'Input/Button Set',
    menuPathes: ['Input/Buttons/'],
    description: 'Groups multiple buttons.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/ButtonSet)',
    properties: {
      add: {
        padding: { placeholder: ['true', 'false'], defaultValue: 'false', mandatory: false, description: "Padding between the buttons", unbindable: true }
      }
    },
    transform (component) {
      // add custom padding class to restProps
      if (component.properties.padding.value === 'true') {
        if (component.properties.class?.value) {
          component.properties.class.value += '+ "avtryck-btn-set-padding"'
        } else {
          component.properties.class = {
            value: '"avtryck-btn-set-padding"',
            bind: false,
            keep: true
          }
        }
      }
      delete component.properties.padding
      return component
    }
  },
  'CopyButton/CopyButton.svelte': {
    id: 'Input/Buttons/Copy Button',
    description: 'A button which will copy the text stated in the text property.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/ButtonSet)',
  },
  'TextInput/TextInput.svelte': {
    id: 'Input/Text Input',
    menuPathes: ['Input/'],
    // name: 'Text',
    description: 'A text input field.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/TextInput)',
    properties: {
      hide: ['ref'],
      add: {
        clearInputButton: { placeholder: ['true', 'false'], defaultValue: 'false', mandatory: false, description: "Clear input value by clicking icon", unbindable: true },
        clearInputButtonText: { placehoder: 'Clear input', defaultValue: '"Clear input"', mandatory: false, description: "Clear input tooltip description", unbindable: true }
      },
      describe: {
        value: {
          placeholder: '',
          defaultValue: '""',
        }
      } 
    },
    slots: {
      hide: ['labelText']
    },
    events: {
      add: {
        clearInput: ''
      }
    },
    transform (component, context, pathes) {
      // a-ui validate on blur (carbon only shows custom errors)
      // input field has now also an unique id
      addInputValidator(component, context)
      // add clear input icon + event
      if (component.properties.clearInputButton?.value === 'true') {
        
        const event = component.eventHandlers.clearInput.value ? component.eventHandlers.clearInput.value : `(event) => {
    if (document.getElementById(${component.properties.id.value})) {
      document.getElementById(${component.properties.id.value}).value = ''
    }
    ${component.properties.value.bind ? component.properties.value.value + " = ''" : ''}
  }`

        context.script.add(`
  const _avtryck_ui_clearInput_transform = (id) => {
    let counter = 0
    let waitForDOM = setInterval(() => {
      const inputField = document.getElementById(id)
      const button = document.getElementById(id + "_clear_button")
      if (inputField && button) {
        clearInterval(waitForDOM)
        inputField.parentNode.appendChild(button)
        // css like password button
        button.classList = "bx--text-input--password__visibility__toggle bx--btn bx--btn--icon-only bx--tooltip__trigger bx--tooltip--a11y bx--tooltip--bottom bx--tooltip--align-end"
      } else if (counter > 100) {
        clearInterval(waitForDOM)
      } else {
        counter++
      }
    }, 100)
  }
`)
        component.slots = {
          labelText: {
            // add labelText again (as it is normally wrapped in this slot)
            components: [
              {
                type: 'code',
                code: `
  <script>
    onMount(() => {
      _avtryck_ui_clearInput_transform(${component.properties.id.value})
    })
  </script>
  {${component.properties.labelText.value}}
  <Button id={${component.properties.id.value} + "_clear_button"} class="avtryck-hide-clearInput-button" kind="ghost" icon={Close} iconDescription={${component.properties.clearInputButtonText?.value} || "Clear input"} tooltipPosition="left" size="field" on:click={${event}} on:keydown={(event) => { if (['Enter', 'NumpadEnter', 'Space'].includes(event.code)) {${event}} }}/>
`
              }
            ],
            use: {}
          }
        }

        // add import and class
        context.imports.add(`import Close from "${pathes.icons}/Close.svelte"`)
        context.imports.add('import Button from "' + pathes.carbonComponents + '/Button/Button.svelte"')
        const buttonStyle = '\n:global(.avtryck-hide-clearInput-button) { display: none !important;}'
        if (!context.style.includes(buttonStyle)) {
          context.style += buttonStyle
        }
      }
      delete component.properties.clearInputButtonText
      delete component.properties.clearInputButton
      return component
    }
  },
  'TextArea/TextArea.svelte': {
    id: 'Input/TextArea Input',
    menuPathes: ['Input/'],
    // name: 'TextArea',
    description: 'A multiline text input field.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/TextArea)',
    properties: {
      hide: ['ref']
    },
    slots: {
      hide: ['labelText']
    },
    transform (component, context, _pathes) {
      addInputValidator(component, context)
      return component
    }
  },
  'NumberInput/NumberInput.svelte': {
    id: 'Input/Number Input',
    menuPathes: ['Input/'],
    // name: 'Number',
    description: 'A number input field.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/NumberInput)',
    properties: {
      hide: ['ref', 'translationIds'], // TODO: Check translationIds
      describe: {
        value: {
          placeholder: '0 or null if no value allowed',
          defaultValue: '',
        }
      }
    },
    slots: {
      hide: ['label']
    },
    transform (component, context, _pathes) {
      if (component.properties.allowEmpty) {
        component.properties.value.defaultValue = null
      } else {
        component.properties.value.defaultValue = 0
      }
      addInputValidator(component, context)
      return component
    }
  },
  'TextInput/PasswordInput.svelte': {
    id: 'Input/Password Input',
    // name: 'Password',
    description: 'A password input field.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/PasswordInput)',
    properties: {
      hide: ['ref'],
      describe: {
        value: {
          placeholder: '',
          defaultValue: '',
        }
      }
    },
    slots: {
      hide: ['labelText']
    },
    transform (component, context, _pathes) {
      addInputValidator(component, context)
      return component
    }
  },
  'Checkbox/Checkbox.svelte': {
    id: 'Input/Checkbox',
    description: 'A checkbox.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Checkbox)',
    properties: {
      hide: ['ref', 'group'] // TODO: Check intention and usage of group
    },
    slots: {
      hide: ['labelText']
    }
  },
  'Toggle/Toggle.svelte': {
    id: 'Input/Toggle',
    menuPathes: ['Input/Buttons/'],
    description: 'A toggle switch.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Toggle)',
    properties: {
      hide: ['labelA', 'labelB', 'labelText']
    }
  },
  'ComboBox/ComboBox.svelte': {
    id: 'Input/Combo Box',
    menuPathes: ['Input/Select/'],
    description: 'A searchable select box.\n\n' + differenceSelectDropdownCombobox + '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/ComboBox)',
    properties: {
      hide: ['ref', 'listRef'] 
    },
    slots: {
      hide: ['default']
    }
  },
  'Dropdown/Dropdown.svelte': {
    id: 'Input/Dropdown',
    menuPathes: ['Input/Select/'],
    description: 'A dropdown selection.\n\n' + differenceSelectDropdownCombobox + '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Dropdown)',
    properties: {
      hide: ['ref', ]
    },
    slots: {
      hide: ['default']
    }
  },
  'DatePicker/DatePicker.svelte': {
    id: 'Input/DatePicker/DatePicker',
    description: 'Date picker outer component.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DatePicker)'
  },
  'DatePicker/DatePickerInput.svelte': {
    id: 'Input/DatePicker/DatePickerInput',
    description: 'Date picker input field. Requires a Date Picker component.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/DatePicker)'
  },
  'Select/Select.svelte': {
    id: 'Input/Select/Select',
    description: 'A select box.\n\n' + differenceSelectDropdownCombobox +  '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Select)',
    slots: {
      hide: ['labelText']
    }
  },
  'Select/SelectItem.svelte': {
    id: 'Input/Select/SelectItem',
    description: 'A select box option. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Select)',
  },
  'Select/SelectItemGroup.svelte': {
    id: 'Input/Select/SelectItemGroup',
    description: 'A select box option group. \n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Select)',
  },
  'MultiSelect/MultiSelect.svelte': {
    id: 'Input/Select/MultiSelect',
    description: 'MultiSelect provides interactivity for a list of checkbox inputs.\n\nWarning: MultiSelect is keyed for performance reasons.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/MultiSelect)',
    properties: {
      describe: {
        sortItem: {
          defaultValue: '' // do not set function into UI as this would lead to errors
        }
      }
    },
    slots: {
      hide: ['titleText']
    }
  }
}
