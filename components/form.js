const { randomUUID } = require('crypto')
const { addValidationVariables } = require('./utils/validatorHelper')

module.exports = {
  // TODO: Add property "auto add group" to wrap everything (except form groups) into a group of their own (for correct spacing)
  'Form/Form.svelte': {
    id: 'Form/Form',
    description: '\n\nIf using custom code on the submit event, use preventDefault (\`function (e) { e.preventDefault() } \`) to avoid errors on submitting.\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Form)',
    properties: {
      hide: ['ref'],
      add: {
        'enable auto-boxing': { placeholder: ['true', 'false'], defaultValue: 'true', mandatory: false, unbindable: true },
        'submit button label': { placeholder: '"Submit"', defaultValue: '"Submit"', mandatory: false, description: 'Will only be shown if an "submit" event is set!', unbindable: true},
        'submit button icon': { placeholder: '', defaultValue: '', mandatory: false, unbindable: true},
        'submit behaviour': { placeholder: ['"auto"', '"once"'], defaultValue: '"once"', mandatory: false, description: '- Auto: Disable submit button for 1 second to avoid triggering multple times. This is usually used if a form can be submitted multiple times.\n\n- Once: Allows to trigger the submit button only one time after client-side-validation has successfully passed. This is usually used if another page will be loaded after submit.\n\n- Custom: Use a boolean variable to keep the submitting button disabled and enable it again manually. This is used if a server-side-validation is needed.', unbindable: true},
        'validation': { placeholder: ['"None"', '"Auto (first error)"', '"Auto (all errors)"'], defaultValue: '"Auto (all errors)"', mandatory: true, description: '- None: Disable validation for this form.\n\n- First error: Validate till first error occurs, then focus on this error.\n\n- All errors: Validate all input fields and shows one or multiple errors. Focus on the first error found.', unbindable: true},
        'disableInputFields': { placeholder: 'Disable all input fields inside this form.\n\nThis is usually only neccessary if submit behaviour is handled by a variable.', placeholder: ['true', 'false'], defaultValue: 'false', mandatory: false, unbindable: true}
      },
    },
    // slots: {
    //   add: ['additionalButtons']
    // },
    transform (component, context, pathes) {
      const autoBoxing = component.properties['enable auto-boxing']?.value !== 'false'
      delete component.properties['enable auto-boxing']
      if (autoBoxing) {
        const defaultComponents = component.slots.default.components
        const newDefaultComponents = []
        defaultComponents.forEach((component) => {
          if (component.name !== '@avtryck-ui-for-p-red/Carbon Design/Form/Group') {
            // Wrap in form group
            const wrappedComponent = {
              type: 'component',
              name: 'Form Group',
              componentId: '@avtryck-ui-for-p-red/Carbon Design/Form/Group',
              properties: {},
              eventHandlers: {},
              slots: {
                default: {
                  components: [component]
                }
              }
            }
            newDefaultComponents.push(wrappedComponent)
          } else {
            newDefaultComponents.push(component)
          }
        })
        component.slots.default.components = newDefaultComponents
      }

      // ensure having validation variables
      addValidationVariables(context)

      const id = component.properties.id?.value ||  ('a_ui_form_' +  randomUUID()).replaceAll('-', '_')
      if (!component.properties.id?.value) {
        component.properties.id = { value: '"' + id + '"' }
      }
      // set default behaviour if non was set
      if (!component.properties['submit behaviour']?.value) {
        component.properties['submit behaviour'] = { value: '"auto"' }
      }
      const autoSubmitHandling = (component.properties['submit behaviour'].value === '"auto"' || component.properties['submit behaviour'].value === '"once"')

      if (component.eventHandlers.submit?.value) {
        // add Renew icon while loading after validation check
        context.imports.add(`import { tick } from 'svelte'`)
        context.imports.add('import Button from "' + pathes.carbonComponents + '/Button/Button.svelte"')
        context.imports.add(`import Renew from "${pathes.icons}/Renew.svelte"`)
        if (component.properties['submit button icon']?.value) {
          context.imports.add(`import ${component.properties['submit button icon'].value} from "${pathes.icons}/${component.properties['submit button icon'].value}.svelte"`)
        }

        context.script.add(`
const _avtryck_ui_validateForm = async (event, formId, validationType) => {
  const form = document.getElementById(formId)
  const stopSubmit = () => {
    // form is invalid - cancel submit
    event.preventDefault()
    event.stopImmediatePropagation()
    // checkbox has no data-invalid currently
    const inputField = getFirstInvalidField()
    inputField?.focus()
    // workaround if checkbox (has no invalid state currently)
    if (inputField.getAttribute('type') === 'checkbox') {
      const label = inputField.parentNode.querySelector('label')
      if (label) {
        label.style.border = '1px solid var(--cds-text-error, #da1e28)'
        label.addEventListener("click", () => { label.style.border = '' }, { once: true})
      }
    }
  }

  const getFirstInvalidField = () => {
    // must get all input fields not only input[data-invalid=true] as checkbox does not have that attribute currently 
    // data-invalid could also be false or a string
    return [...form.querySelectorAll('input')].find(i => !i.validity.valid || !!i.getAttribute('data-invalid'))
  }

  if (validationType !== "None") {
    // already existing error and single error mode
    if (validationType === "Auto (first error)" && getFirstInvalidField()) {
      stopSubmit()
      return
    }

    // focus each field to trigger blur event (showing missed fields)
    const inputs = form.querySelectorAll('input')
    for (let index = 0; index < inputs.length; index++) {
      inputs[index].focus()
      await tick()
      form.focus() // ensure trigger last input blur or if Auto (first error) stop when first error found
      await tick()
      if (validationType === "Auto (first error)" && getFirstInvalidField()) {
        stopSubmit()
        return
      }
    }

    // checkValidity is necessary as e.g. checkbox can be required, but won't be set to invalid (currently)
    if (!form.checkValidity() || getFirstInvalidField()) {
      stopSubmit()
    }
  }
}

// ssr first page has a form can lead to error as document is not defined yet
let _avtryck_ui_form_mounted = false
onMount(() => {
  _avtryck_ui_form_mounted = true
})
const _avtryck_ui_setFormInputDisabled = (id, setTo) => {
  if (!_avtryck_ui_form_mounted) {
    return
  }
  const inputs = [...document?.getElementById(id)?.querySelectorAll('input'), ...document?.getElementById(id)?.querySelectorAll('textarea')]
  if (!inputs) {
    // form not ready yet
    return
  }
  if (setTo === true) {
    inputs.forEach(input => {
      if (!input.getAttribute('disabled')) {
        // add a_ui attribute to prevent enabling other disabled fields
        input.setAttribute('a_ui_submitting', 'true')
        input.setAttribute('disabled', 'true')
      }
    })
  } else {
    inputs.forEach(input => {
      if (input.getAttribute('a_ui_submitting') === 'true') {
        input.removeAttribute('a_ui_submitting')
        input.removeAttribute('disabled')
      }
    })
  }
}
`)
        // use a dynamic created variable to control submit behaviour
        if (autoSubmitHandling) {
          context.bindVar.set(id + '_submitting', false)
        }
        if (!component.properties.disableInputFields.value || component.properties.disableInputFields.value === 'false') {
          component.properties.disableInputFields.value = false
        }
        component.slots.default.components.push(
          {
            type: 'code',
            code: `
            <script>
              $: if((${autoSubmitHandling ? id + '_submitting': component.properties['submit behaviour'].value}) || ${component.properties.disableInputFields.value}) {
                _avtryck_ui_setFormInputDisabled("${id}", true)
              } else {
                _avtryck_ui_setFormInputDisabled("${id}", false)
              }
            </script>
            <Button type="submit"
              class={${(autoSubmitHandling ? id + '_submitting': component.properties['submit behaviour'].value)} ? "spinning" : ""}
              icon={${(autoSubmitHandling ? id + '_submitting' : component.properties['submit behaviour'].value)} ? Renew : ${component.properties["submit button icon"]?.value}} 
              iconDescription={${component.properties['submit button label']?.value || 'Submit'}} 
              on:click={(event) => _avtryck_ui_validateForm(event, "${id}", ${component.properties['validation']?.value})} 
              disabled={${autoSubmitHandling ? id +'_submitting' : component.properties['submit behaviour'].value}}
            >
              {${component.properties['submit button label']?.value || 'Submit'}}
            </Button>
            `
          }
        )
      }

      // add preventDefault to submit (this will be triggered after validating the form)
      if (component.eventHandlers.submit?.value && component.eventHandlers.submit.type !== 'custom') {
        component.eventHandlers.submit.before = 'event.preventDefault(); ' + (autoSubmitHandling ? id + '_submitting' : component.properties['submit behaviour'].value) + ' = true;'
        if (component.properties['submit behaviour'].value === '"auto"') {
          component.eventHandlers.submit.after = 'setTimeout(() => ' + id + '_submitting = false, 1000);'
        }
      }

      // no validation wanted -> add property novalidation
      if (component.properties['validation']?.value === '"None"') {
        component.properties.novalidate = { value: 'true' }
      }
      
      delete component.properties['submit button label']
      delete component.properties['submit button icon']
      delete component.properties['submit behaviour']
      delete component.properties['validation']
      return component
    }
  },
  // This seems not to be ready yet. Thus we will postpone integration into p-red.
  // https://github.com/carbon-design-system/carbon/issues/12453
  // https://github.com/carbon-design-system/carbon-components-svelte/issues/502
  //
  // 'FluidForm/FluidForm.svelte': {
  //   id: 'Form/Fluid Form',
  //   description: 'A fluid form.' // TODO: Replace with link to official documentation
  // },
  'FormGroup/FormGroup.svelte': {
    id: 'Form/Group',
    description: '\n\n[Official documentation](https://carbon-components-svelte.onrender.com/components/Form)'
  }
}
