// this variable and function must be added to all input fields and to the form.
// it will only be added once to the page
const addValidationVariables = (context) => {
  // a variable to add errors for validation
  context.bindVar.set('_avtryck_ui_input_validation', {})
  // add our validator function if not existing
  const validatorFunction = `
  const _avtryck_ui_input_validator = (id) => {
    const inputField = document.getElementById(id)
    if (!inputField.validity.valid) {
      _avtryck_ui_input_validation[id] = inputField.validationMessage
    } else {
      _avtryck_ui_input_validation[id] = '' // delete would not be recognized by svelte
    };
  }
`
  context.script.add(validatorFunction)
}

module.exports = {
  addValidationVariables
}