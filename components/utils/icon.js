const resolvePath = require('../../utils/module-resolver')

const wrapIcon = (icon = '', context, pathes) => {
  if (icon) {
    let icons = [icon]
    if (icon.includes('?') && icon.includes(':')) {
      icons = icon.split('?')[1].split(':').map(i => i.trim())
    }
    icons.forEach(i => {
      if (resolvePath('carbon-icons-svelte/lib/' + i + '.svelte')) {
        context.imports.add(`import ${i} from "${pathes.icons}/${i}.svelte"`)
      }
    })
  }
}


module.exports = {
  wrapIcon
}