const { createRequire } = require('module')
const fs = require('fs')
const path = require('path')

/**
 * Resolves a module's folder.
 * @param {string} moduleEntry
 * @param {string} fromFile
 */
const getModulePath = (moduleEntry, relativeToFile = __filename) => {
  const packageName = moduleEntry.includes('/')
    ? moduleEntry.startsWith('@')
      ? moduleEntry.split('/').slice(0, 2).join('/')
      : moduleEntry.split('/')[0]
    : moduleEntry
  const require = createRequire(relativeToFile)
  const lookupPaths = require.resolve.paths(moduleEntry).map((p) => path.join(p, packageName))
  const moduleFolder = lookupPaths.find((p) => fs.existsSync(p))
  if (moduleFolder) {
    return path.join(moduleFolder.substring(0, moduleFolder.length - packageName.length), moduleEntry)
  }
  return undefined
}

module.exports = getModulePath
