const wrapDescription = `// Wrap the page content into a grid structure.
// - auto: Wrap the whole slot content into one Row -> Column -> "slot components" structure or completes the structure if one component is missing. 
// - Row: Wrap the whole slot content into a Row component.
// - Row & Column: Wrap the whole slot content into a Row -> Column -> "slot components" structure.
// - none: Do not wrap.`

module.exports = {
  './App.svelte': {
    id: 'App',
    description: 'An App layout with header, (optional) sidebar and actions. \n\n[Official documentation](https://carbondesignsystem.com/components/UI-shell-header/usage)',
    properties: {
      add: {
         // keep wrap here to prevent console log warning unused property
        'wrap': { placeholder: ['"auto"', '"Row"', '"Row & Column"', '"none"'], defaultValue: '"auto"', mandatory: false, description: wrapDescription, unbindable: true }
      }
    },
    transform (component) {
      const defaultComponents = component.slots.default.components
      let wrap = component.properties.wrap?.value
      if (!['"auto"', '"Row"', '"Row & Column"', '"none"'].includes(wrap)) {
        wrap = '"none"'
      }
      wrap = wrap.slice(1, -1)
      delete component.properties.wrap

      const wrapDefaultComponentsInRow = () => {
        component.slots.default.components = [
          {
            type: 'component',
            name: 'Row',
            componentId: '@avtryck-ui-for-p-red/Carbon Design/Grid/Row',
            properties: {},
            eventHandlers: {},
            slots: {
              default: {
                components: defaultComponents
              }
            }
          }
        ]
      }


      const wrapDefaultComponentsInRowAndColumn = () => {
        component.slots.default.components = [
          {
            type: 'component',
            name: 'Row',
            componentId: '@avtryck-ui-for-p-red/Carbon Design/Grid/Row',
            properties: {},
            eventHandlers: {},
            slots: {
              default: {
                components: [
                  {
                    type: 'component',
                    name: 'Column',
                    componentId: '@avtryck-ui-for-p-red/Carbon Design/Grid/Column',
                    properties: {},
                    eventHandlers: {},
                    slots: {
                      default: {
                        components: defaultComponents
                      }
                    }
                  }
                ]
              }
            }
          }
        ]
      }

      // TODO: We do not handle the case that some components are wrapped in a Column and some are not.
      // As this does not seem to be a good design choice, we will ignore it for now and wait for a valid use-case.
      if (wrap === 'auto') {
        const rowIndex = defaultComponents.findIndex(component => component.componentId === '@avtryck-ui-for-p-red/Carbon Design/Grid/Row')
        const row = defaultComponents[rowIndex]
        const columnIndex = defaultComponents.findIndex(component => component.componentId === '@avtryck-ui-for-p-red/Carbon Design/Grid/Column')
        const column = defaultComponents[columnIndex]
        if (row) {
          // if the Row has at least one column in his slot, we do nothing
          if (!row.slots.default.components.find(comp => comp.componentId === '@avtryck-ui-for-p-red/Carbon Design/Grid/Column' )) {
            // Wrap Row slot components in Column
            row.slots.default.components = [{
              type: 'component',
              name: 'Column',
              componentId: '@avtryck-ui-for-p-red/Carbon Design/Grid/Column',
              properties: {},
              eventHandlers: {},
              slots: {
                default: {
                  components: row.slots.default.components
                }
              }
            }]
          }
          return component
        } else if (column) {
          // Wrap default slot in in Row
          wrapDefaultComponentsInRow()
        } else {
          // Wrap slot content in Row and Column
          wrapDefaultComponentsInRowAndColumn()
        }
      } else if (wrap === 'Row') {
        // Wrap slot content in in Row
        wrapDefaultComponentsInRow()
      } else if (wrap === 'Row & Column') {
        // Wrap slot content in Row and Column
        wrapDefaultComponentsInRowAndColumn()
      }
      return component
    }
  }
}
