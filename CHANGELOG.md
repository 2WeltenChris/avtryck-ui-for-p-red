> **Attention:** ⚠️ means that a change breaks things. Manual adjustments will be necessary. So be careful before updating. Even data loss might occur.

**Version 1.3.0 (23rd of September 2024)**
- input validate text fix
- import / export slot for App / Blank layout

**Version 1.2.3 (15th of August 2024)**
- path to css var fix

**Version 1.2.2 (15th of August 2024)**
- added carbon css variables to the theme files to ensure avtryck custom components still use the correct color
- added carbon-css-variables as globals (web-app)

**Version 1.2.1 (05th of August 2024)**
- sidebar disable font-weight and rmv background color

**Version 1.2.0 (31th of July 2024)**
- Allow 2 icons (cond ? icon1 : icon2 )
- SideNavEntry additional classes fix
- disable shell header + sidebar menu added
- Notification event propagation fix
- added Button spinning icon 
- added disable input fields for form
