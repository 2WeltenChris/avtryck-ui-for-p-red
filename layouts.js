const app = require('./layouts/app.js')
const blank = require('./layouts/blank.js')

module.exports = { ...app, ...blank }
