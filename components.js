const shell = require('./components/shell.js')
const input = require('./components/input.js')
const grid = require('./components/grid.js')
const form = require('./components/form.js')
const navigation = require('./components/navigation.js')
const notification = require('./components/notification.js')
const modal = require('./components/modal.js')
const tile = require('./components/tile.js')
const misc = require('./components/misc.js')
const table = require('./components/table.js')

module.exports = { ...shell, ...input, ...grid, ...form, ...navigation, ...notification, ...modal, ...tile, ...misc, ...table }